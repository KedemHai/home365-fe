interface Loader {
  show(): void;

  hide(): void;
}

export {Loader};
