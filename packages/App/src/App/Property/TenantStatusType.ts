enum TenantStatusType {
  Active,
  Inactive
}

export default TenantStatusType;
