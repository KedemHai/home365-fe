enum PropertyStatusType {
  Active,
  Occupied,
  Vacant,
  Inactive
}

export default PropertyStatusType;
