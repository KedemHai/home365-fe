interface PropertyTenantDto {
  id: string;
  name: string;
  status: string;
  active: boolean;
}

export {PropertyTenantDto};
