import {PropertyOwnerDto} from './PropertyOwnerDto';
import {PropertyTenantDto} from './PropertyTenantDto';

interface PropertyDto {
  id: string;
  created: Date;
  address: string;
  status: string;
  occupied: boolean;
  vacant: boolean;
  inactive: boolean;
  plan: string;
  owner: PropertyOwnerDto;
  tenant?: PropertyTenantDto;
}

export {PropertyDto};
