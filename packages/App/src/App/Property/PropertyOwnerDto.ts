interface PropertyOwnerDto {
  name: string;
  status: string;
  active: boolean;
}

export {PropertyOwnerDto};
