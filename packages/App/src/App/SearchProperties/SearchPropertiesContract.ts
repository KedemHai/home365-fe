import PropertyStatusType from '../Property/PropertyStatusType';
import TenantStatusType from '../Property/TenantStatusType';
import {PropertyDto} from '../Property/PropertyDto';

interface SearchPropertiesInput {
  execute(
      request: SearchPropertiesRequest, output: SearchPropertiesOutput): void;
}

class SearchPropertiesRequest {
  propertyStatus?: PropertyStatusType;
  tenantStatus?: TenantStatusType;
}

interface SearchPropertiesOutput {
  displayPropertyList(list: PropertyDto[]): void;
}

export {SearchPropertiesInput, SearchPropertiesRequest, SearchPropertiesOutput};
