import {DemoDataPropertyDto} from './DemoDataDto';
import {PropertyOwnerDto} from '../../App/Property/PropertyOwnerDto';

class PropertyOwnerDtoFromDemoData implements PropertyOwnerDto {
  private readonly data: DemoDataPropertyDto;

  private constructor(data: DemoDataPropertyDto) {
    this.data = data;
  }

  public get active(): boolean {
    return this.data.ownerStatus === 'active';
  }

  public get name(): string {
    return this.data.owner;
  }

  public get status(): string {
    return this.data.ownerStatus;
  }

  public static from(data: DemoDataPropertyDto): PropertyOwnerDtoFromDemoData {
    return new PropertyOwnerDtoFromDemoData(data);
  }
}

export default PropertyOwnerDtoFromDemoData;
