import {DemoDataPropertyDto} from './DemoDataDto';
import {PropertyDto} from '../../App/Property/PropertyDto';
import {PropertyTenantDto} from '../../App/Property/PropertyTenantDto';
import PropertyTenantDtoFromDemoData from './PropertyTenantDtoFromDemoData';
import {PropertyOwnerDto} from '../../App/Property/PropertyOwnerDto';
import PropertyOwnerDtoFromDemoData from './PropertyOwnerDtoFromDemoData';

class PropertyDtoFromDemoData implements PropertyDto {
  private readonly data: DemoDataPropertyDto;

  private constructor(data: DemoDataPropertyDto) {
    this.data = data;
  }

  public get address(): string {
    return this.data.address;
  }

  public get created(): Date {
    return new Date(this.data.createdOn);
  }

  public get id(): string {
    return this.data.propertyId;
  }

  public get inactive(): boolean {
    return this.data.occupiedStats === 'inactive';
  }

  public get occupied(): boolean {
    return this.data.occupiedStats === 'occupied';
  }

  public get owner(): PropertyOwnerDto {
    return PropertyOwnerDtoFromDemoData.from(this.data);
  }

  public get plan(): string {
    return this.data.plan;
  }

  public get status(): string {
    return this.data.occupiedStats;
  }

  public get tenant(): PropertyTenantDto | undefined {
    if (this.data.tenant === null) {
      return;
    }
    return PropertyTenantDtoFromDemoData.from(this.data.tenant);
  }

  public get vacant(): boolean {
    return this.data.occupiedStats === 'vacant';
  }

  public static from(data: DemoDataPropertyDto): PropertyDtoFromDemoData {
    return new PropertyDtoFromDemoData(data);
  }
}

export default PropertyDtoFromDemoData;
