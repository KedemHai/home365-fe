import {DemoDataTenantDto} from './DemoDataDto';
import {PropertyTenantDto} from '../../App/Property/PropertyTenantDto';

class PropertyTenantDtoFromDemoData implements PropertyTenantDto {
  private readonly data: DemoDataTenantDto;

  private constructor(data: DemoDataTenantDto) {
    this.data = data;
  }

  public get active(): boolean {
    return this.data.tenantStatus === 'active';
  }

  public get id(): string {
    return this.data.contactId;
  }

  public get name(): string {
    let name = `${this.data.firstName}`;
    if (this.data.lastName !== null) {
      name = `${name} ${this.data.lastName}`;
    }
    return name;
  }

  public get status(): string {
    return this.data.tenantStatus;
  }

  public static from(data: DemoDataTenantDto): PropertyTenantDtoFromDemoData {
    return new PropertyTenantDtoFromDemoData(data);
  }
}

export default PropertyTenantDtoFromDemoData;
