interface DemoDataDto {
  properties: DemoDataPropertyDto[];
}

interface DemoDataPropertyDto {
  propertyId: string;
  createdOn: string;
  address: string;
  occupiedStats: 'occupied' | 'vacant' | 'inactive';
  owner: string;
  ownerStatus: 'active';
  tenant: DemoDataTenantDto | null;
  plan: string;
}

interface DemoDataTenantDto {
  contactId: string;
  firstName: string;
  lastName: string | null;
  tenantStatus: 'active' | 'inactive';
}

export {DemoDataDto, DemoDataPropertyDto, DemoDataTenantDto};
