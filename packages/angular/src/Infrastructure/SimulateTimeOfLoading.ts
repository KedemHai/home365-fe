import {
  SearchPropertiesInput,
  SearchPropertiesOutput,
  SearchPropertiesRequest,
} from 'home365-app/dist/App/SearchProperties/SearchPropertiesContract';
import {Deferred} from 'ts-deferred';
import {PropertyDto} from 'home365-app/dist/App/Property/PropertyDto';
import SearchPropertiesInputDecorator from './SearchPropertiesInputDecorator';

class SimulateTimeOfLoading extends SearchPropertiesInputDecorator {
  private deferredLoading: Deferred<void> | null = null;

  public constructor(delegate: SearchPropertiesInput) {
    super(delegate);
  }

  public override async execute(
      request: SearchPropertiesRequest,
      output: SearchPropertiesOutput): Promise<void> {
    if (this.deferredLoading !== null) {
      await this.deferredLoading.promise;
    }
    this.deferredLoading = new Deferred<void>();
    setTimeout(() => {
      (this.deferredLoading as Deferred<void>).resolve();
    }, 2000);
    super.execute(request, output);
  }

  public override async displayPropertyList(list: PropertyDto[]): Promise<void> {
    if (this.deferredLoading !== null) {
      await this.deferredLoading.promise;
    }
    this.deferredLoading = null;
    super.displayPropertyList(list);
  }
}

export default SimulateTimeOfLoading;
