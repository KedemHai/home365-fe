import {HttpClient} from '@angular/common/http';
import {
  SearchPropertiesInput,
  SearchPropertiesOutput,
  SearchPropertiesRequest,
} from 'home365-app/dist/App/SearchProperties/SearchPropertiesContract';
import {
  DemoDataDto,
} from 'home365-app/dist/Infrastructure/DemoData/DemoDataDto';
import PropertyDtoFromDemoData
  from 'home365-app/dist/Infrastructure/DemoData/PropertyDtoFromDemoData';
import {lastValueFrom} from 'rxjs';
import PropertyStatusType
  from 'home365-app/dist/App/Property/PropertyStatusType';
import TenantStatusType from 'home365-app/dist/App/Property/TenantStatusType';

class SearchPropertiesWithHttp implements SearchPropertiesInput {
  private readonly httpClient: HttpClient;

  public constructor(httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  public async execute(
      request: SearchPropertiesRequest,
      output: SearchPropertiesOutput): Promise<void> {
    const httpRequest = this.httpClient.get<DemoDataDto>(
        '/assets/properties.json', {
          observe: 'body',
        });
    const data = await lastValueFrom(httpRequest);

    if (typeof request.propertyStatus !== 'undefined') {
      data.properties = data.properties.filter((value) => {
        switch (request.propertyStatus) {
          case PropertyStatusType.Active:
            return (
                value.occupiedStats === 'occupied' ||
                value.occupiedStats === 'vacant'
            );
          case PropertyStatusType.Inactive:
            return value.occupiedStats === 'inactive';
          case PropertyStatusType.Occupied:
            return value.occupiedStats === 'occupied';
          case PropertyStatusType.Vacant:
            return value.occupiedStats === 'vacant';
          default:
            return false;
        }
      });
    }

    if (typeof request.tenantStatus !== 'undefined') {
      data.properties = data.properties.filter((value) => {
        switch (request.tenantStatus) {
          case TenantStatusType.Active:
            return (
                value.tenant !== null &&
                value.tenant.tenantStatus === 'active'
            );
          case TenantStatusType.Inactive:
            return (
                value.tenant !== null &&
                value.tenant.tenantStatus === 'inactive'
            );
          default:
            return false;
        }
      });
    }

    output.displayPropertyList(
        data.properties.map((value) => PropertyDtoFromDemoData.from(value)));
  }
}

export default SearchPropertiesWithHttp;
