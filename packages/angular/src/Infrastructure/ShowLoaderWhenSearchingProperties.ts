import {
  SearchPropertiesInput,
  SearchPropertiesOutput,
  SearchPropertiesRequest,
} from 'home365-app/dist/App/SearchProperties/SearchPropertiesContract';
import {Loader} from 'home365-app/dist/UI/Loader/Loader';
import {PropertyDto} from 'home365-app/dist/App/Property/PropertyDto';
import SearchPropertiesInputDecorator from './SearchPropertiesInputDecorator';

class ShowLoaderWhenSearchingProperties extends SearchPropertiesInputDecorator {
  private readonly loader: Loader;

  public constructor(delegate: SearchPropertiesInput, loader: Loader) {
    super(delegate);
    this.loader = loader;
  }

  public override execute(
      request: SearchPropertiesRequest, output: SearchPropertiesOutput): void {
    this.loader.show();
    super.execute(request, output);
  }

  public override displayPropertyList(list: PropertyDto[]): void {
    this.loader.hide();
    super.displayPropertyList(list);
  }
}

export default ShowLoaderWhenSearchingProperties;
