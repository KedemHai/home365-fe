import {
  SearchPropertiesInput,
  SearchPropertiesOutput,
  SearchPropertiesRequest,
} from 'home365-app/dist/App/SearchProperties/SearchPropertiesContract';
import {PropertyDto} from 'home365-app/dist/App/Property/PropertyDto';

abstract class SearchPropertiesInputDecorator implements SearchPropertiesInput, SearchPropertiesOutput {
  private readonly delegate: SearchPropertiesInput;
  private output: SearchPropertiesOutput | null = null;

  protected constructor(delegate: SearchPropertiesInput) {
    this.delegate = delegate;
  }

  public execute(
      request: SearchPropertiesRequest, output: SearchPropertiesOutput): void {
    this.output = output;
    this.delegate.execute(request, this);
  }

  public displayPropertyList(list: PropertyDto[]): void {
    if (this.output === null) {
      throw new Error('Unexpected null output');
    }
    this.output.displayPropertyList(list);
  }
}

export default SearchPropertiesInputDecorator;
