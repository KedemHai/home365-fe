import {Component, Inject} from '@angular/core';
import {PropertyDto} from 'home365-app/dist/App/Property/PropertyDto';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-property-tenant-details-dialog',
  templateUrl: './property-tenant-details-dialog.component.html',
  styleUrls: ['./property-tenant-details-dialog.component.scss'],
})
export class PropertyTenantDetailsDialogComponent {
  public readonly property: PropertyDto;

  public constructor(@Inject(MAT_DIALOG_DATA) property: PropertyDto) {
    this.property = property;
  }
}
