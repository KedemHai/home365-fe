import {Component, EventEmitter, Input, Output} from '@angular/core';
import {PropertyDto} from 'home365-app/dist/App/Property/PropertyDto';

@Component({
  selector: 'app-property-table',
  templateUrl: './property-table.component.html',
  styleUrls: ['./property-table.component.scss'],
})
export class PropertyTableComponent {
  @Input() properties: PropertyDto[] = [];
  @Output() propertyAddressClicked: EventEmitter<PropertyDto> = new EventEmitter<PropertyDto>();
  @Output() propertyOwnerClicked: EventEmitter<PropertyDto> = new EventEmitter<PropertyDto>();
  @Output() propertyTenantClicked: EventEmitter<PropertyDto> = new EventEmitter<PropertyDto>();

  public displayedColumns = [
    'created',
    'address',
    'status',
    'plan',
    'owner',
    'owner-status',
    'tenant',
    'tenant-status'];

  public trackByPropertyId(index: number, property: PropertyDto): string {
    return `${property.id}`;
  }

  public hasTenant(property: PropertyDto): boolean {
    return typeof property.tenant !== 'undefined';
  }
}
