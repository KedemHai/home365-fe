import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {PropertyDto} from 'home365-app/dist/App/Property/PropertyDto';

@Component({
  selector: 'app-property-details-dialog',
  templateUrl: './property-details-dialog.component.html',
  styleUrls: ['./property-details-dialog.component.scss'],
})
export class PropertyDetailsDialogComponent {
  public readonly property: PropertyDto;

  public constructor(@Inject(MAT_DIALOG_DATA) property: PropertyDto) {
    this.property = property;
  }
}
