import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-format-date',
  templateUrl: './format-date.component.html',
  styleUrls: ['./format-date.component.scss'],
})
export class FormatDateComponent {
  @Input() date: Date | null = null;

  public get format(): string {
    if (this.date === null) {
      return '';
    }
    return new Intl.DateTimeFormat('en-US').format(this.date);
  }
}
