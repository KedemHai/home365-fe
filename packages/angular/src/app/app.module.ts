import {Injector, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PropertiesComponent} from './properties/properties.component';
import {
  PropertyTableComponent,
} from './property-table/property-table.component';
import {FormsModule} from '@angular/forms';
import {SearchPropertiesInputInject} from '../injection-tokens';
import {
  SearchPropertiesInput,
} from 'home365-app/dist/App/SearchProperties/SearchPropertiesContract';
import SearchPropertiesWithHttp
  from '../Infrastructure/SearchPropertiesWithHttp';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import SimulateTimeOfLoading from '../Infrastructure/SimulateTimeOfLoading';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatTableModule} from '@angular/material/table';
import {FormatDateComponent} from './format-date/format-date.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import {
  PropertyDetailsDialogComponent,
} from './property-details-dialog/property-details-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {
  PropertyOwnerDetailsDialogComponent,
} from './property-owner-details-dialog/property-owner-details-dialog.component';
import {
  PropertyTenantDetailsDialogComponent,
} from './property-tenant-details-dialog/property-tenant-details-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    PropertiesComponent,
    PropertyTableComponent,
    FormatDateComponent,
    PropertyDetailsDialogComponent,
    PropertyOwnerDetailsDialogComponent,
    PropertyTenantDetailsDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressBarModule,
    MatTableModule,
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
  ],
  providers: [
    {
      provide: SearchPropertiesInputInject,
      useFactory: (injector: Injector) => {
        const httpClient: HttpClient = injector.get<HttpClient>(HttpClient);
        let searchPropertyInput: SearchPropertiesInput;
        searchPropertyInput = new SearchPropertiesWithHttp(httpClient);
        searchPropertyInput = new SimulateTimeOfLoading(searchPropertyInput);
        return searchPropertyInput;
      },
      deps: [Injector],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
