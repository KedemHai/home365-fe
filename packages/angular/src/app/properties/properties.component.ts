import {Component, Inject, OnInit} from '@angular/core';
import {
  SearchPropertiesInput,
  SearchPropertiesOutput,
  SearchPropertiesRequest,
} from 'home365-app/dist/App/SearchProperties/SearchPropertiesContract';
import {PropertyDto} from 'home365-app/dist/App/Property/PropertyDto';
import {SearchPropertiesInputInject} from '../../injection-tokens';
import PropertyStatusType
  from 'home365-app/dist/App/Property/PropertyStatusType';
import TenantStatusType from 'home365-app/dist/App/Property/TenantStatusType';
import {Loader} from 'home365-app/dist/UI/Loader/Loader';
import PropertiesComponentLoader from './PropertiesComponentLoader';
import ShowLoaderWhenSearchingProperties
  from '../../Infrastructure/ShowLoaderWhenSearchingProperties';
import {MatDialog} from '@angular/material/dialog';
import {
  PropertyDetailsDialogComponent,
} from '../property-details-dialog/property-details-dialog.component';
import {
  PropertyOwnerDetailsDialogComponent,
} from '../property-owner-details-dialog/property-owner-details-dialog.component';
import {
  PropertyTenantDetailsDialogComponent,
} from '../property-tenant-details-dialog/property-tenant-details-dialog.component';

@Component({
  selector: 'app-properties',
  templateUrl: './properties.component.html',
  styleUrls: ['./properties.component.scss'],
})
export class PropertiesComponent implements OnInit, SearchPropertiesOutput {
  public isLoading: boolean = false;
  public listOfProperties: PropertyDto[] = [];
  public searchRequest: {
    propertyStatus: string;
    tenantStatus: string;
  } = {
    propertyStatus: '',
    tenantStatus: '',
  };
  private readonly searchProperties: SearchPropertiesInput;
  private readonly dialog: MatDialog;

  constructor(
      @Inject(
          SearchPropertiesInputInject) searchProperties: SearchPropertiesInput,
      dialog: MatDialog) {
    let loader: Loader = new PropertiesComponentLoader(this);
    this.searchProperties = new ShowLoaderWhenSearchingProperties(
        searchProperties, loader);
    this.dialog = dialog;
  }

  public ngOnInit(): void {
    this.requestSearchProperties();
  }

  public displayPropertyList(list: PropertyDto[]): void {
    this.listOfProperties = list;
  }

  public onSearchRequestChange() {
    this.requestSearchProperties();
  }

  public onPropertyAddressClick(property: PropertyDto) {
    this.dialog.open(PropertyDetailsDialogComponent, {
      data: property,
    });
  }

  public onPropertyOwnerClick(property: PropertyDto) {
    this.dialog.open(PropertyOwnerDetailsDialogComponent, {
      data: property,
    });
  }

  public onPropertyTenantClick(property: PropertyDto) {
    this.dialog.open(PropertyTenantDetailsDialogComponent, {
      data: property,
    });
  }

  private requestSearchProperties() {
    const request = new SearchPropertiesRequest();
    switch (this.searchRequest.propertyStatus) {
      case 'active':
        request.propertyStatus = PropertyStatusType.Active;
        break;
      case 'occupied':
        request.propertyStatus = PropertyStatusType.Occupied;
        break;
      case 'vacant':
        request.propertyStatus = PropertyStatusType.Vacant;
        break;
      case 'inactive':
        request.propertyStatus = PropertyStatusType.Inactive;
        break;
    }

    switch (this.searchRequest.tenantStatus) {
      case 'active':
        request.tenantStatus = TenantStatusType.Active;
        break;
      case 'inactive':
        request.tenantStatus = TenantStatusType.Inactive;
        break;
    }

    this.searchProperties.execute(request, this);
  }
}
