import {Loader} from 'home365-app/dist/UI/Loader/Loader';
import {PropertiesComponent} from './properties.component';

class PropertiesComponentLoader implements Loader {
  private readonly component: PropertiesComponent;

  public constructor(component: PropertiesComponent) {
    this.component = component;
  }

  public hide(): void {
    this.component.isLoading = false;
  }

  public show(): void {
    this.component.isLoading = true;
  }
}

export default PropertiesComponentLoader;
