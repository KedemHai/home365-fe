import {InjectionToken} from '@angular/core';
import {
  SearchPropertiesInput,
} from 'home365-app/dist/App/SearchProperties/SearchPropertiesContract';

export const SearchPropertiesInputInject: InjectionToken<SearchPropertiesInput> = new InjectionToken<SearchPropertiesInput>(
    'SearchPropertiesInput Interface');
