# Home365

## How to run

1. Install Node.Js and npm
2. install Yarn globally

```shell
npm install -g yarn
```

3. Run the start script

```shell
yarn start
```

this will install all dependencies and serve the application.

4. Once the application is running it will be available on http://localhost:4200
